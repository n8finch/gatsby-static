import React from 'react'
import Link from 'gatsby-link'
import Helmet from 'react-helmet'
import Banner from '../components/Banner'

import pic01 from '../assets/images/pic01.jpg'
import pic02 from '../assets/images/pic02.jpg'
import pic03 from '../assets/images/pic03.jpg'
import pic04 from '../assets/images/pic04.jpg'
import pic05 from '../assets/images/pic05.jpg'
import pic06 from '../assets/images/pic06.jpg'

class HomeIndex extends React.Component {
    render() {
        const siteTitle = this.props.data.site.siteMetadata.title
        const siteDescription = this.props.data.site.siteMetadata.description

        return (
            <div>
                <Helmet>
                    <title>{siteTitle}</title>
                    <meta name="description" content={siteDescription} />
                </Helmet>

                <Banner />

                <div id="main">
                    <section id="one" className="tiles">
                        <article style={{ backgroundImage: `url(${pic01})` }}>
                            <header className="major">
                                <h3>A new level, a slightly different direction…</h3>
                                <p>I've been freelancing on my own for a little over three years. It's been great, but I was feeling stagnate. So, I've decided to go in a slightly different direction...</p>
                            </header>
                            <Link to="/landing" className="link primary"></Link>
                        </article>
                        <article style={{ backgroundImage: `url(${pic02})` }}>
                            <header className="major">
                                <h3>Joining a Team, Seeking Community...</h3>
                                <p>I've decided to join a few teams moving forward. Over the last year, I began longing for community in my development journey. I wanted to be a part of something bigger than myself. To join something where the the whole was greater than the sum of its parts.</p>
                            </header>
                            <Link to="/landing" className="link primary"></Link>
                        </article>
                        <article className="full-width" style={{ backgroundImage: `url(${pic03})` }}>
                            <header className="major">
                                <h3>What am I doing now??</h3>
                                <p>You can find me doing work with these awesome folks...</p>
								<p>If you're looking for custom development and ongoing consulting over the long term, you should contract me on toptal.</p>
								<p>If you're looking for custom development and WordPress services for the short-term or a smaller, one-off project, you should hire me on Codeable.</p>
                            </header>
                            <Link to="/landing" className="link primary"></Link>
                        </article>
                        <article style={{ backgroundImage: `url(${pic04})` }}>
                            <header className="major">
                                <h3>I still answer email…</h3>
                                <p>If you need anything or have any questions, please don't hesitate to reach out to me via email.</p>
                            </header>
                            <Link to="/landing" className="link primary"></Link>
                        </article>
                        <article style={{ backgroundImage: `url(${pic05})` }}>
                            <header className="major">
                                <h3>A little about me…</h3>
                                <p>WordPress full-stack developer, mostly front-end, (trying to) follow Jesus, married & loving it. MBA | MA | M.Div... Blogs at n8finch.com</p>
                            </header>
                            <Link to="/landing" className="link primary"></Link>
                        </article>
                    </section>
                    <section id="two">
                        <div className="inner">
                            <header className="major">
                                <h2>Massa libero</h2>
                            </header>
                            <p>Nullam et orci eu lorem consequat tincidunt vivamus et sagittis libero. Mauris aliquet magna magna sed nunc rhoncus pharetra. Pellentesque condimentum sem. In efficitur ligula tate urna. Maecenas laoreet massa vel lacinia pellentesque lorem ipsum dolor. Nullam et orci eu lorem consequat tincidunt. Vivamus et sagittis libero. Mauris aliquet magna magna sed nunc rhoncus amet pharetra et feugiat tempus.</p>
                            <ul className="actions">
                                <li><Link to="/landing" className="button next">Get Started</Link></li>
                            </ul>
                        </div>
                    </section>
                </div>

            </div>
        )
    }
}

export default HomeIndex

export const pageQuery = graphql`
    query PageQuery {
        site {
            siteMetadata {
                title
                description
            }
        }
    }
`
