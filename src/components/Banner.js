import React from 'react'

const Banner = (props) => (
    <section id="banner" className="major">
        <div className="inner">
            <header className="major">
                <h1>Hi, my name Nate Finch</h1>
            </header>
            <div className="content">
                <p>I'm a full-stack developer, mostly front-end, mostly WordPress.<br />
                    I'm always learning new and fun things.</p>
                <ul className="actions">
                    <li><a href="#one" className="button next scrolly">More about me...</a></li>
                </ul>
            </div>
        </div>
    </section>
)

export default Banner
